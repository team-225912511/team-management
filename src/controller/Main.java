package controller;
import domain.model.*;
import java.util.*;
import static controller.UsersMenu.DeveloperMenu.*;
import static controller.UsersMenu.LeadMenu.*;
import static controller.UsersMenu.ManagerMenu.*;
import static controller.UsersMenu.TesterMenu.testerMenu;
import static domain.model.UserRole.*;

public class Main implements Utils{

    public static int cmd;
    public static void main(String[] args) {
        defData();
        menu();
    }

    public static void menu() {
        System.out.println("\n 1. SIGN UP\n 2. SIGN IN\n 0. EXIT");
        System.out.print("\n Choose : ");
        cmd = scanNum.nextInt();
        switch (cmd) {
            case 0 -> {}
            case 1 -> signUp();
            case 2 -> signIn();
        }
    }

    public static void signUp() {
        System.out.print("Enter your name : ");
        String name= scanStr.nextLine();
        System.out.print("Enter username : ");
        String username = scanStr.nextLine();
        System.out.print("Enter password : " );
        String password = scanStr.nextLine();
        User user = new User(name, username, password,USER);
        if(userService.add(user) == 1){
            System.out.println("Successfully signed");
            System.out.println("Wait until level up\n\n");
        }
        else {
            System.out.println("This user already exist");
        }
        menu();
    }
    public static void signIn(){
        System.out.print("\n Enter username : ");
        String username = scanStr.nextLine();
        System.out.print(" Enter password : ");
        String password = scanStr.nextLine();
        User user = userService.signIn(username,password);
        if(!Objects.equals(user,null)){
            UserRole role = user.getRole();
            switch (role){
                case MANAGER -> {
                    managerMenu(user);
                }
                case BEKENDDEV,FRONTENDDEV -> {
                    developerMenu(user);
                }
                case FRONTENDLEAD,BECKENDLEAD -> {
                    leadMenu(user);
                }
                case TESTER -> {
                    testerMenu(user);
                }
                default -> {
                    System.out.println("\n You are only user wait until level up by Manager");
                }
            }
        }
        else {
            System.out.println("\n Password or username is wrong");
        }

    }
    public static void defData(){
        userService.add(new User("MANAGER","user1","123",MANAGER));
        userService.add(new User("BACKENDLEAD","user2","123",BECKENDLEAD));
        userService.add(new User("FRONTENDLEAD","user3","123",FRONTENDLEAD));
        userService.add(new User("BACKENDDEV","user4","123",BEKENDDEV));
        userService.add(new User("FRONTENDDEV","user5","123",FRONTENDDEV));
    }



}