package controller;

import domain.model.User;
import service.task.TaskService;
import service.task.TaskServiceImpl;
import service.user.UserService;
import service.user.UserServiceImpl;

import java.util.ArrayList;
import java.util.Scanner;

public interface Utils {
    Scanner scanNum = new Scanner(System.in);
    Scanner scanStr = new Scanner(System.in);
    UserService userService = UserServiceImpl.getInstance();
    TaskService taskService = TaskServiceImpl.getInstance();
    ArrayList<User> USERS = userService.showAll();
}
