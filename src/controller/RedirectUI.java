package controller;
import domain.model.*;

import static controller.UsersMenu.DeveloperMenu.developerMenu;
import static controller.UsersMenu.LeadMenu.leadMenu;
import static controller.UsersMenu.ManagerMenu.managerMenu;

public class RedirectUI {
    public static void userMenu(User user){
        UserRole role = user.getRole();
        switch (role){
            case USER -> {
                System.out.println("Successfully signed!");
            }
            case MANAGER -> {
                managerMenu(user);
            }
            case BECKENDLEAD,FRONTENDLEAD -> {
                leadMenu(user);
            }
            case BEKENDDEV, FRONTENDDEV -> {
                developerMenu(user);
            }

        }
    }







}
