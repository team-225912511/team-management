package controller.UsersMenu;

import controller.Utils;
import domain.model.*;
import static controller.Main.*;
import static domain.model.UserRole.*;

public class ManagerMenu implements Utils {
    public static void managerMenu(User user) {
        System.out.println("\n 1. Create task");
        System.out.println(" 2. See users");
        System.out.println(" 3. Change user role");
        System.out.println(" 4. Show all tasks");
        System.out.println(" 0. Back");
        System.out.print("\n Choose: ");
        cmd = scanNum.nextInt();
        switch (cmd){
            case 1 -> {
                createTask(user);
            }
            case 2 ->{
                seeUsers(user);
            }
            case 3 -> {
                changeUserRole(user);
            }
            case 4 ->{
                showAllTasks(user);
            }
            case 0 ->{
                menu();
            }
        }
    }

    private static void showAllTasks(User currentUser) {
        int k = 1;
        for (Task t: taskService.showAll()) {
            User thisUser = userService.getById(t.getOrderId());
            System.out.println(k++ + ". " + thisUser.getName() + ",  " + t.getTitle() + ", " + t.getStatus());

        }
        System.out.println("\n");
        managerMenu(currentUser);
    }

    protected static void seeUsers(User currentUser) {
        int k = 1;
        for (User u : USERS) {
            System.out.println(k++ + ". " + u.getName() + ", " + u.getRole());
        }
        System.out.println("\n");
        managerMenu(currentUser);
    }


    private static void createTask(User currentUser){
        TaskMenu.createTask(currentUser);
        System.out.println("\nSuccessfully created");
        System.out.println("\n");
        managerMenu(currentUser);

    }
    private static void changeUserRole(User currentUser) {
            int k = 1;
            for (User u : USERS) {
                System.out.println(k++ + ". " + u.getName() + ", " + u.getRole());
                }
             System.out.print("Choose: ");
             int cmd = scanNum.nextInt();
             User user = USERS.get(cmd-1);
             if(taskService.checkOfTaskUser(user)){
                 System.out.println("Select role below");
                 System.out.println("1. Backend developer");
                 System.out.println("2. Fronted developer");
                 System.out.println("3. Backend leader");
                 System.out.println("4. Frontend leader");
                 System.out.println("5. Tester");
                 System.out.println("0. Back");
                 System.out.print("Choose: ");
                 cmd = scanNum.nextInt();
                 switch (cmd) {
                     case 1 -> {
                         userService.changeRole(user,BEKENDDEV);
                     }
                     case 2 -> {
                         userService.changeRole(user,FRONTENDDEV);
                     }
                     case 3 -> {
                         userService.changeRole(user,BECKENDLEAD);
                     }
                     case 4 -> {
                         userService.changeRole(user,FRONTENDLEAD);
                     }
                     case 5 -> {
                         userService.changeRole(user,TESTER);
                     }
                 }
                 System.out.println("\n Successfully changed");
             }
             else {
                     System.out.println("This user already get task");
                     System.out.println("You cannot change role");
                     System.out.println("Choose other one");

             }
            managerMenu(currentUser);
        }

    }

