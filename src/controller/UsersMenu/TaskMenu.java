package controller.UsersMenu;

import controller.Utils;
import domain.model.Task;
import domain.model.User;

import java.util.UUID;
import static controller.UsersMenu.ManagerMenu.*;
import static domain.model.TaskStatus.*;

public class TaskMenu implements Utils {
    public static void createTask(User user){
        System.out.print("\n Enter title: ");
        String title = scanStr.nextLine();
        System.out.print(" Enter description: ");
        String description = scanStr.nextLine();
        int k = 1;
        for (User u : USERS) {
            System.out.println(k++ + ". " + u.getName() + ", " + u.getRole());
        }
        System.out.print("\n Choose: ");
        int n = scanNum.nextInt();
        UUID userId = USERS.get(n-1).getId();
        Task newTask = new Task(title,description,userId,CREATED);
        taskService.add(newTask);
    }

}
