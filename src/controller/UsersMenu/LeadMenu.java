package controller.UsersMenu;

import controller.Utils;
import domain.model.Task;
import domain.model.TaskStatus;
import domain.model.User;

import java.util.ArrayList;

import static controller.Main.menu;
import static domain.model.TaskStatus.*;

public class LeadMenu implements Utils {

    public static void leadMenu(User user){
        System.out.println("\n 1. TASKS\n 2. DO TASK\n 3. TASKS IN PROGRESS\n 4. DONE TASKS\n 0. BACK");
        System.out.print("\n Choose : ");
        int cmd = scanNum.nextInt();
        switch (cmd){
            case 0 -> menu();
            case 1 -> tasks(user);
            case 2 -> doTask(user);
            case 3 -> taskInProgress(user);
            case 4 -> doneTasks(user);
        }
    }

    private static void doneTasks(User user) {
        ArrayList<Task> tasks = taskService.getTasksByStatus(user.getId(), DONE);
        if(tasks.size() == 0){
            System.out.println("\n Tasks not found!");
            leadMenu(user);
        }
        int i = 1;
        for (Task task : tasks) {
            System.out.println(" " + i++ + ". " + task.getTitle() + " | " + task.getDescription());
        }
        leadMenu(user);
    }

    private static void taskInProgress(User user) {
        ArrayList<Task> tasks = taskService.getTasksByStatus(user.getId(), IN_PROGRESS);
        if(tasks.size() == 0){
            System.out.println("\n Tasks not found!");
            leadMenu(user);
        }
        System.out.println();
        int i = 1;
        for (Task task : tasks) {
            System.out.println(" " + i++ + ". " + task.getTitle() + " | " + task.getDescription());
        }
        System.out.print("\n Choose : ");
        int index = scanNum.nextInt();
        index--;
        System.out.println("\n\n CONTINUE THE ANSWER");
        System.out.print(" " + tasks.get(index).getAnswer());
        String answer = scanStr.nextLine();
        tasks.get(index).setAnswer(tasks.get(index).getAnswer() + answer);
        System.out.println("\n 1. TASK COMPLETION\n 2. LEAVE FOR LATER");
        System.out.print("\n Choose : ");
        int cmd = scanNum.nextInt();
        switch (cmd){
            case 1 -> taskCompletion(tasks.get(index));
            case 2 -> {}
        }
        leadMenu(user);
    }

    private static void doTask(User user) {
        ArrayList<Task> tasks = taskService.getTasksByStatus(user.getId(), ASSIGNED);
        if(tasks.size() == 0){
            System.out.println("\n Tasks not found!");
            leadMenu(user);
        }
        System.out.println();
        int i = 1;
        for (Task task : tasks) {
            System.out.println(" " + i++ + ". " + task.getTitle() + " | " + task.getDescription());
        }
        System.out.print("\n Choose : ");
        int index = scanNum.nextInt();
        index--;
        tasks.get(index).setStatus(IN_PROGRESS);
        System.out.print("\n Enter the answer : ");
        String answer = scanStr.nextLine();
        tasks.get(index).setAnswer(answer);
        System.out.println("\n 1. TASK COMPLETION\n 2. LEAVE FOR LATER");
        System.out.print("\n Choose : ");
        int cmd = scanNum.nextInt();
        switch (cmd){
            case 1 -> taskCompletion(tasks.get(index));
            case 2 -> {}
        }
        leadMenu(user);
    }

    private static void taskCompletion(Task task) {
        task.setStatus(TaskStatus.DONE);
    }

    private static void tasks(User user) {
        ArrayList<Task> tasks = taskService.getTasksByStatus(user.getId(), CREATED);
        if(tasks.size() == 0){
            System.out.println("\n Tasks not found!");
            leadMenu(user);
        }
        System.out.println();
        int i = 1;
        for (Task task : tasks) {
            System.out.println(" " + i++ + ". " + task.getTitle() + " | " + task.getDescription());
        }
        System.out.print("\n Choose : ");
        int index = scanNum.nextInt();
        index--;
        System.out.println("\n " + tasks.get(index).getTitle() + " | " + tasks.get(index).getDescription());
        System.out.println("\n 1. Acceptance\t 2. Send off");
        System.out.print("\n Choose : ");
        int cmd = scanNum.nextInt();
        switch (cmd){
            case 1 -> acceptTask(tasks.get(index));
            case 2 -> sendOff(user, tasks.get(index));
        }
        System.out.println("\n Completed successfully");
        leadMenu(user);
    }

    private static void sendOff(User currentUser, Task task) {
        ArrayList<User> users = userService.getDev(currentUser.getRole());
        int i = 1;
        for (User user : users) {
            System.out.println(i++ + ". " + user.getName() + " | " + user.getUsername());
        }
        System.out.print("\n Choose : ");
        int index = scanNum.nextInt();
        index--;
        task.setOrderId(users.get(index).getId());
    }

    private static void acceptTask(Task task) {
        task.setStatus(TaskStatus.ASSIGNED);
    }
}
