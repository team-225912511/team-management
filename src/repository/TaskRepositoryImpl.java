package repository;

import domain.model.Task;

import java.util.ArrayList;
import java.util.UUID;

public class TaskRepositoryImpl implements TaskRepository{
    private final ArrayList<Task> TASK_LIST = new ArrayList<>();
    
    private static final TaskRepositoryImpl instance = new TaskRepositoryImpl();

    public static TaskRepositoryImpl getInstance() {
        return instance;
    }
    private TaskRepositoryImpl(){
    }
    
    @Override
    public int save(Task task) {
        TASK_LIST.add(task);
        return 1;
    }

    @Override
    public Task getById(UUID id) {
        return null;
    }

    @Override
    public ArrayList<Task> getAll() {
        return TASK_LIST;
    }

}
