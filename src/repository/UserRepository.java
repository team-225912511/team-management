package repository;

import domain.model.User;

public interface UserRepository extends BaseRepository<User>{
}
