package repository;

import domain.model.User;
import java.util.ArrayList;
import java.util.UUID;

public class UserRepositoryImpl implements UserRepository{
    private final ArrayList<User> USER_LIST = new ArrayList<>();
    private static final UserRepositoryImpl instance = new UserRepositoryImpl();

    public static UserRepositoryImpl getInstance() {
        return instance;
    }
    private UserRepositoryImpl(){
    }

    @Override
    public int save(User user) {
        USER_LIST.add(user);
        return 1;
    }

    @Override
    public User getById(UUID id) {
        return null;
    }

    @Override
    public ArrayList<User> getAll() {
        return USER_LIST;
    }
}
