package repository;

import domain.model.Task;

public interface TaskRepository extends BaseRepository<Task>{
}
