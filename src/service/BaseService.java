package service;


import java.util.ArrayList;

public interface BaseService<CD> {
    int add(CD cd);
    ArrayList<CD> showAll();

}

