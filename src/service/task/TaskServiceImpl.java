package service.task;

import com.sun.source.util.TaskListener;
import domain.model.Task;

import domain.model.TaskStatus;
import domain.model.User;
import repository.TaskRepository;
import repository.TaskRepositoryImpl;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

import static domain.model.TaskStatus.IN_PROGRESS;

public class TaskServiceImpl implements TaskService{
    static TaskRepositoryImpl taskRepository = TaskRepositoryImpl.getInstance();
    private final ArrayList<User> userService = new ArrayList<>();
    private static final TaskServiceImpl instance = new TaskServiceImpl();

    public static TaskServiceImpl getInstance() {
        return instance;
    }
    private TaskServiceImpl(){
    }

    @Override
    public int add(Task task) {
        taskRepository.save(task);
        return 1;
    }

    @Override
    public ArrayList<Task> showAll() {
        ArrayList<Task> tasks = new ArrayList<>();
        for(Task t: taskRepository.getAll()){
            tasks.add(t);
        }
        return tasks;
    }

    @Override
    public ArrayList<Task> getTaskBlock() {
        ArrayList<Task> tasks = new ArrayList<>();
        for(Task t : taskRepository.getAll()){
            if(Objects.equals(t.getStatus(), TaskStatus.BLOCK)){
                tasks.add(t);
            }
        }
        return tasks;
    }
    @Override
    public ArrayList<Task> getCreatedTask() {
        ArrayList<Task> tasks = new ArrayList<>();
        for(Task t: taskRepository.getAll()){
            if(Objects.equals(t.getStatus(), TaskStatus.CREATED)){
                tasks.add(t);
            }
        }
        return tasks;
    }

    @Override
    public ArrayList<Task> getTasksByStatus(UUID id, TaskStatus status) {
            ArrayList<Task> tasks = new ArrayList<>();
            for(Task t : taskRepository.getAll()){
                if(Objects.equals(t.getStatus(), status)
                        && Objects.equals(t.getOrderId(), id)){
                    tasks.add(t);
                }
            }
            return tasks;
    }


    @Override
    public ArrayList<Task> getCanseled() {
        ArrayList<Task> tasks = new ArrayList<>();
        for(Task t : taskRepository.getAll()){
            if(Objects.equals(t.getStatus(), TaskStatus.CANCELED)){
                tasks.add(t);
            }
        }
        return tasks;
    }



    @Override
    public boolean checkOfTaskUser(User user) {

        for(Task task: getTasksByStatus(user.getId(), IN_PROGRESS)){

            if(Objects.equals(task.getOrderId(), user.getId())){
                return false;
            }
        }
        return true;
    }



    @Override
    public ArrayList<Task> taskOfUser(UUID id) {
        ArrayList<Task> tasks = new ArrayList<>();
        for(Task t: taskRepository.getAll()){
            if(Objects.equals(t.getId(), id)){
                tasks.add(t);
            }
        }
        return tasks;
    }

    @Override
    public ArrayList<Task> getTaskById(UUID id) {
        ArrayList<Task> task = new ArrayList<>();
        for(Task t: taskRepository.getAll()){
            if(Objects.equals(t.getId(), id)){
                task.add(t);
            }
        }
        return task;
    }


    @Override
    public void changeTaskBlock(UUID id){
        for(Task t: taskRepository.getAll()){
            if(Objects.equals(t.getId(), id)){
                t.setStatus(TaskStatus.BLOCK);
            }
        }
    }
}
