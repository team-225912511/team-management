package service.task;

import domain.model.Task;
import domain.model.TaskStatus;
import domain.model.User;
import service.BaseService;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.UUID;

public interface TaskService extends BaseService<Task> {
    ArrayList<Task> getTaskBlock();
    ArrayList<Task> getCreatedTask();
    ArrayList<Task> getTasksByStatus(UUID id, TaskStatus status);

    ArrayList<Task> getCanseled();
    boolean checkOfTaskUser(User user);
    void changeTaskBlock(UUID id);

    ArrayList<Task> taskOfUser(UUID id);
    ArrayList<Task> getTaskById(UUID id);


}
