package service.user;


import domain.model.Task;
import domain.model.User;
import domain.model.UserRole;
import repository.UserRepositoryImpl;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class UserServiceImpl implements UserService{

    static UserRepositoryImpl userRepository = UserRepositoryImpl.getInstance();
    private final ArrayList<User> userService = new ArrayList<>();
    private static final UserServiceImpl instance = new UserServiceImpl();

    public static UserServiceImpl getInstance() {
        return instance;
    }
    private UserServiceImpl(){
    }
    @Override
    public int add(User user) {
        if(doesPasswordExists(user.getUsername())){
            return -1;
        }
        userRepository.save(user);
        return 1;
    }

    private boolean doesPasswordExists(String username) {
        for(User u: userRepository.getAll()){
            if(Objects.equals(u.getUsername(),username )){
                return true;
            }
        }
        return false;
    }

    @Override
    public ArrayList<User> showAll() {
        return userRepository.getAll();
    }


    @Override
    public User getById(UUID id) {
        for(User u : userRepository.getAll()){
            if(Objects.equals(u.getId(), id)){
                return u;
            }
        }
        return null;
    }

    @Override
    public ArrayList<User> getDev(UserRole role) {
        ArrayList<User> dev = new ArrayList<>();
        if(Objects.equals(role, UserRole.BECKENDLEAD)){
            for(User u: userRepository.getAll()){
                if(Objects.equals(u.getRole(), UserRole.BEKENDDEV)){
                    dev.add(u);
                }
            }
        }else if(Objects.equals(role, UserRole.FRONTENDLEAD)){
            for(User u: userRepository.getAll()){
                if(Objects.equals(u.getRole(), UserRole.FRONTENDDEV)){
                    dev.add(u);
                }
            }
        }
        return dev;
    }

    @Override
    public User signIn(String username, String password) {
        for(User user: userRepository.getAll()){
            if(Objects.equals(user.getUsername(), username)
                    && Objects.equals(user.getPassword(), password)){
                return user;
            }
        }
        return null;
    }


    @Override
    public void changeRole(User user, UserRole role) {
        for(User u : userRepository.getAll()){
            if(Objects.equals(u.getId(), user.getId())){
                u.setRole(role);
            }
        }
    }

    @Override
    public ArrayList<User> allUser() {
        ArrayList<User> users = new ArrayList<>();
        for(User u: userRepository.getAll()){
            if(Objects.equals(u.getRole(), UserRole.USER)){
                users.add(u);
            }
        }
        return users;
    }

    @Override
    public ArrayList<User> allDev() {
        ArrayList<User> users = new ArrayList<>();
        for(User u: userRepository.getAll()){
            if(!(Objects.equals(u.getRole(), UserRole.USER))){
                users.add(u);
            }
        }
        return users;
    }




}
