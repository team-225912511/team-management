package service.user;
import domain.model.*;
import service.*;
import java.util.*;

public interface UserService extends BaseService<User> {
    void changeRole(User user, UserRole role);
    ArrayList<User> allUser();
    ArrayList<User> allDev();
    User getById(UUID id);
    ArrayList<User> getDev(UserRole role);
    User signIn(String username, String password);
}
