package domain.model;

import java.util.UUID;

public class Task extends BaseModel {
    private String title;
    private String description;
    private String answer;
    private UUID orderId;
    private TaskStatus status;

    public Task(String title, String description, UUID orderId, TaskStatus status) {
        this.title = title;
        this.description = description;
        this.orderId = orderId;
        this.status = status;
    }

    public Task() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public UUID getOrderId() {
        return orderId;
    }

    public void setOrderId(UUID orderId) {
        this.orderId = orderId;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }
}
