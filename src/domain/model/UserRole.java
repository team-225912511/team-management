package domain.model;

public enum UserRole {
    MANAGER,
    BECKENDLEAD,
    FRONTENDLEAD,
    BEKENDDEV,
    FRONTENDDEV,
    TESTER,
    USER
}
