package domain.DTO;

public record SignUpDTO(String name,String username, String password){
}
